require('dotenv').config();
const fs = require("fs");
const { parse } = require("csv-parse");
const cheerio = require('cheerio');
var http = require('http');
const https = require('node:https');
const { log, count } = require("console");
const fse = require('fs-extra');
const Path = require('path');


let missionCSVFile = "./src/Giggi - Uppdrags.csv";
let agendaCSVfile = "./src/frågedrivnauppdrag-safe - Agenda måls.csv";
let missionTypeCSVFile = "./src/frågedrivnauppdrag-safe - Typ av uppdrags.csv";

let  outputPath = process.env.OUTPUT_PATH? process.env.OUTPUT_PATH : "./public";
if (!process.env.OUTPUT_PATH){
    process.env.OUTPUT_PATH = outputPath;
}

async function writeFile (fileName, fileContent){
    let count = 0;
    
    console.log(fileName)
    await fs.writeFile(fileName, fileContent, (err) => {
        if (err && count> 15) {
            count++;
            console.log(count)
            writeFile(fileName,fileContent);
        };
        console.log('The file has been saved:  ' + fileName);
      });
}


function adMeets($,$element,mission){ 
    $meets = $element.find(".del2");    
    $meets = $meets.find(".datumwrapper")
    j=1
    $meets.each(function(i, elm) {
        
        
        
        date = new Date(mission['meetup'+j+'Start']);
        dateEnd = new Date(mission['meetup'+j+'End']);
        durration = (dateEnd.getTime()- date.getTime()) /1000/60;
        
        $(elm).parent().parent().addClass("date-container")
        $(elm).parent().parent().attr("data-date",date)
        $(elm).parent().parent().attr("data-durration",durration)

        $(elm).find(".datum1 > h2").text(date.getDate());
        $(elm).find(".datum-2 > h6").text(date.toLocaleString('default', { month: 'short' }));
        j+=1

    });
}

function addDate(id,value, $){
    date = new Date(value);
    
    if(date != "Invalid Date"){
        $("#"+id).attr("date",value)
    
        $("#"+id+" .datum1 .heading-5").text(date.getDate());
        $("#"+id+" .datum-2 h6").text(date.toLocaleString('default', { month: 'short' }));
        $("#"+id).parent().children(".tidwrapper").children(".tidwrapper_step2").children(".tid").children(".tidb-rjar").text(date.toLocaleString('default', {hour: '2-digit', minute:'2-digit'}))
    }else{
        $("#"+id).parent().remove().html();
    }
}

var downloadimg = function(url, dest, cb) {
    try {
        var file = fs.createWriteStream(dest);
        
        https.get(url, function(response) {
          response.pipe(file);
          file.on('finish', function() {
            file.close(cb);
          });
        });
    } catch (error) {
        
    }

  }



function download(url, dest, cb) {
    try {
        console.log(dest+"                                <------")
        if (!fs.existsSync(dest)) {
           
            
            https.get(url, (res) => {
                
                res.on('data', (d) => {
                    
                    fs.appendFileSync(dest, d);
                    
                  });
                  

            })
    
              
          }
       
    } catch (error) {
        console.log(error)
    }

}
fs.rmSync(outputPath, { recursive: true, force: true });
  if (!fs.existsSync(outputPath)){
    fs.mkdirSync(outputPath);
}
  let dir = outputPath+"/icons/";
  if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}
try {
    
    fse.copySync("./src", outputPath, { overwrite: false })
    
  } catch (err) {
    console.error(err)
  }
try {
fse.copySync("./customjs", outputPath+"/js", { overwrite: true })
} catch (err) {
console.error(err)
}
let taskType = []
let goals= []



  



fs.createReadStream(agendaCSVfile)
        .pipe(parse({ delimiter: ",", from_line: 2 }))
        .on("data", function (row) {
            goals.push(row)
            downloadimg(row[8], outputPath+"/icons/"+row[1]+".png")

        })
        .on("end", function () {
            fs.createReadStream(missionTypeCSVFile)
            .pipe(parse({ delimiter: ",", from_line: 2 }))
            .on("data", function (row) {
                taskType.push(row)
                downloadimg(row[7],outputPath+"/icons/"+row[1]+".png")

            })
            .on("end", function () {
                
                
                purgeExternal().then(mainFunc())
                
                
                
                
                
                
                
            })
            .on("error", function (error) {
                console.log(error.message);
            });
        })
        .on("error", function (error) {
            console.log(error.message);
        });




let missions=[];
let keys = [];

function mapObject(values) {
    let missionObj = {}
    if(keys.length == 0) {
        for (i = 0; values.length > i; i++){
            keys = values;
        }

    }else{      
        for (i = 0; values.length > i; i++){
            missionObj[keys[i]] = values[i];
        }
        missions.push(missionObj);
    }
}
async function mainFunc(){      
    
    keys = [];
    fs.createReadStream(missionCSVFile)
        .pipe(parse({ delimiter: ",", from_line: 1 }))
        .on("data", function (row) {
            mapObject(row)
            
        })
        .on("end", function () {
            for(let i= 0; i < keys.length; i++){
                //console.log(i+"   "+keys[i])
            }
           for (let mission of missions){
            //let mission = missions[index];
            
            
            fs.readFile("./src/detail_uppdrag.html", 'utf8', async (err, data) => {
                if (err) {
                  console.error(err);
                  return;
                }
                const $ = cheerio.load(data);
                adMeets($,$("body").first(),mission)
                $("#w-node-ed31958f-1d6e-435d-a01f-614810bcfdbe-143fa1e9").text(mission.leadingQuestion); // drivande fråga
        
                
                $(".del3 p:first").text(mission.description);
    //            $("head").append('<script src="./js/searchpage.js" type="text/javascript"></script>')
                $("head").append('<script src="./js/custom.js" type="text/javascript"></script>');
                
                if(mission.leadingQuestion2 != ""){
                    $("#w-node-_96c320c8-5960-7f85-cabc-7f61c90d4df3-143fa1e9 .flerdrivandefragor").text(mission.leadingQuestion2);
                    if(mission.leadingQuestion3 != ""){
                        $("#w-node-_96c320c8-5960-7f85-cabc-7f61c90d4df4-143fa1e9 .flerdrivandefragor").text(mission.leadingQuestion3);
                    }else{
                        $("#w-node-_96c320c8-5960-7f85-cabc-7f61c90d4df4-143fa1e9 .flerdrivandefragor").parent().parent().remove();
                    }
                    if(mission.leadingQuestion4 != ""){
                        $("#w-node-_96c320c8-5960-7f85-cabc-7f61c90d4df5-143fa1e9 .flerdrivandefragor").text(mission.leadingQuestion4);
                    }else{
                        $("#w-node-_96c320c8-5960-7f85-cabc-7f61c90d4df5-143fa1e9 .flerdrivandefragor").parent().parent().remove();
                    }
                }else{
                    $("#w-node-_96c320c8-5960-7f85-cabc-7f61c90d4df3-143fa1e9 .flerdrivandefragor").parent().parent().parent().remove();
                }

                $("#w-node-e6e47bdb-3235-702f-71d5-2baac340534f-143fa1e9").text(mission.leadingQuestionAgendaMainGoal);

                addDate("w-node-_04ddbe87-4467-ed20-10fa-77421e1d3b17-143fa1e9", mission.meetup1Start, $);
                addDate("w-node-_35c36f7e-1ec7-ea38-bd15-6f88883b4388-143fa1e9", mission.meetup1Start, $);
                addDate("w-node-_137bde93-799d-16bd-6fbe-bbb449bcfce4-143fa1e9", mission.meetup2Start, $);
                addDate("w-node-eedf0f5e-78ea-6b8d-67e3-298eee420f85-143fa1e9", mission.meetup2Start, $);
                addDate("w-node-_8bc2b2b1-e800-5dbe-0880-f653f7777320-143fa1e9", mission.meetup3Start, $);
                addDate("w-node-_2ccf6c9b-c29e-f81e-0a84-e41f84966cb8-143fa1e9", mission.meetup3Start, $);
                addDate("w-node-_4642e42a-d510-fe00-633e-069a3dd0e0a6-143fa1e9", mission.meetup4Start, $);
                addDate("w-node-ea542e4c-731e-9074-6346-6bbde765ba04-143fa1e9", mission.meetup4Start, $);
                
                $(".typwrapper").html('<img src="./icons/'+mission.gigType+'.png" loading="lazy" width="60" alt="">');
                
                for(i = 0; i<taskType.length; i++){
                    if(taskType[i][1] == mission.gigType){
                        $(".typwrapper").append('<div class="typ_text">'+taskType[i][0]+'</div>');
                    }
                }
                let tsdada= await writeFile(outputPath+"/"+mission.Slug+".html", $.html())

            });
        
           }
           

           
        })
        .on("error", function (error) {
            console.log(error.message);
        });
        
        
        
        

        fs.readFile(outputPath+"/sok-uppdrag.html", 'utf8', (err, sokPage) => {


            const $ = cheerio.load(sokPage);
            

            



            $searchList = $("#w-node-f37b10d7-f1e7-4198-75d2-66972b73763c-203fa1f4").clone();
            
            for(i = 0; taskType.length > i; i++){
                $searchList.find("img").first().attr("src","./icons/"+taskType[i][1]+".png");
                $searchList.find(".infoboxtext").first().text(taskType[i][1]);
                $("#w-node-f37b10d7-f1e7-4198-75d2-66972b73763c-203fa1f4").parent().append($searchList.clone());
            }
            $("#w-node-f37b10d7-f1e7-4198-75d2-66972b73763c-203fa1f4").first().remove();

            $goalList = $("#w-node-_8a9c8b91-6b89-e0d5-6dfb-f103411ede5e-203fa1f4").clone();
            
            for(i = 0; goals.length > i; i++){
                
                $goalList.find("img").first().attr("src","./icons/"+goals[i][1]+".png");
                $goalList.find(".infoboxtext").first().text(goals[i][9]);
                $("#w-node-_8a9c8b91-6b89-e0d5-6dfb-f103411ede5e-203fa1f4").parent().append($goalList.clone());
            }
            $("#w-node-_8a9c8b91-6b89-e0d5-6dfb-f103411ede5e-203fa1f4").first().remove();


            $("head").append('<script src="./js/searchpage.js"></script>');
            sokDiv = $("#w-node-e761a2e7-d231-8853-8f3b-ea36e224967c-203fa1f4").clone();
            
            for(let mission of missions){
                //let mission = missions[index];  
              
            
            sokDiv.attr('id', function(i, id){
                return id=mission.Slug;
               
            });
            
            
            $("#w-node-e761a2e7-d231-8853-8f3b-ea36e224967c-203fa1f4").parent().append(sokDiv.clone());
            $element = $("#"+mission.Slug);
            $element.addClass(mission.gigType);
            $element.find(".linktilluppdrag").first().attr("href","./"+mission.Slug+".html");
            $element.find(".hj-lps-kare").text(mission.organization);
            $element.find("#w-node-_91cfcfb4-114e-cbb8-c88d-4912710bad3a-203fa1f4").text(mission.leadingQuestion);

            if(mission.scopeHours != ''){
                $element.find(".timeplacebox__text-timmar").first().text(mission.scopeHours);
            }else{
                $element.find(".infoboxtext.tim").first().addClass("w-condition-invisible")
            }
            if(mission.scopeDays != ''){
                $element.find(".timeplacebox__text-dagar").first().text(mission.scopeDays);
            }else{
                $element.find(".infoboxtext.days").first().addClass("w-condition-invisible")
            }
            if(mission.scopeMonths != ''){
                $element.find(".timeplacebox__text-months").first().text(mission.scopeMonths);
            }else{
                $element.find(".infoboxtext.months").first().addClass("w-condition-invisible")
            }
            
 
            


            // möten

            adMeets($,$element,mission);



            $element.find(".typwrapper > img").first().attr("src", "./icons/"+mission.gigType+".png");
            $element.find(".typwrapper > .typ_text").first().text(mission.gigType);
            $element.find(".hidden_filter_typ").first().text(mission.gigType);
            for(let i=0;goals.length > i; i++){
                
                if(mission.agendaMainGoal = goals[i][1]){
                    $element.find(".hidden_filter_agenda").first().text(goals[i][9]);
                    break;
                }
            }
            
            
            
        }
                
            $(".uppdragwrapper > .w-dyn-list > .w-dyn-empty").first().remove();
    
            $(".uppdragwrapper > .w-dyn-list").first().append($(".empty_wrapper").first().clone());
            $(".empty_wrapper").first().remove();

            $("#w-node-e761a2e7-d231-8853-8f3b-ea36e224967c-203fa1f4").remove();
            
            writeFile(outputPath+"/sok-uppdrag.html", $.html());
        
            
        console.log("finished generate pages");
           
        });

        

        

    
 
}


function  purgeExternal() {
    
    const fs = require('fs');
    

    

    fs.readdir(outputPath, async (err, files) => {
    files.forEach(file => {
        
        if (Path.extname(file) =='.html'){
            fs.readFile(outputPath+"/"+file, 'utf8', async (err, data) => {
                if (err) {
                  console.error(err);
                  return;
                }
                $ = cheerio.load(data)
                
                let scriptFilesElements = $("script")
                for(let i = 0; i<scriptFilesElements.length;i++){
                    let src = $(scriptFilesElements[i]).attr("src");
                    let fileName = src;
                    if(fileName != undefined){
                        while(fileName.indexOf("/")!=-1 ){
                        
                            fileName = fileName.substring(fileName.indexOf("/")+1,fileName.length)
                        }
    
                        while(fileName.indexOf("?")!=-1){
                            
                            fileName = fileName.substring(0,fileName.indexOf("?"))
                            
                            
                        }
                    
    
                    if(fileName != ""){
                        
                        $(scriptFilesElements[i]).attr("src","./js/"+fileName)
                        $(scriptFilesElements[i]).removeAttr("integrity")
                        
                        if (src.includes("http")){
                            download(src, outputPath+"/js/"+fileName);

                            
    
                        }
    
                        
    
                    }
                    
                }
                
               
                
                }
                
                    await writeFile(outputPath+"/"+file, $.html());
                   
                
    
                    return new Promise((resolve) => {
                        resolve;
                    })
                
                
            });
        }
                    

        
    });
    return new Promise((resolve) => {
        resolve;
    })
    });

    console.log("finished purge external src")
    console.log(taskType)
    return new Promise((resolve) => {
        resolve;
    })
}

    
    
  
 



  