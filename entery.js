require('dotenv').config();
const fs = require("fs");
const { parse } = require("csv-parse");
const cheerio = require('cheerio');
var http = require('http');
const https = require('node:https');
const { log, count } = require("console");
const fse = require('fs-extra');
const Path = require('path');


let missionCSVFile = "./src/Giggi - Uppdrags.csv";
let agendaCSVfile = "./src/Giggi - AgendaGoals.csv";
let missionTypeCSVFile = "./src/Giggi - Typ av uppdrags.csv";
let missions=[];
let missionTypes = [];
let goals = [];
let keysMissionCSVFile = [];
let keysAgendaCSVfile = [];
let keysMissionTypeCSVFile = [];
let pages = []
let fileExceptions = ["detail_uppdrag.html", "sok-uppdrag.html"]


let  outputPath = process.env.OUTPUT_PATH? process.env.OUTPUT_PATH : "./public";
if (!process.env.OUTPUT_PATH){
    process.env.OUTPUT_PATH = outputPath;
}


function writeFile (fileName, fileContent){
    let count = 0;
    
    
    fs.writeFile(fileName, fileContent, (err) => {
        if (err && count> 15) {
            count++;
            
            writeFile(fileName,fileContent);
        };
        console.log('The file has been saved:  ' + fileName);
      });
}



function mapObject(values, missions, keys) {
    let missionObj = {}
    if(keys.length == 0) {
        for (i = 0; values.length > i; i++){
            keys.push(values[i])
        }
    }else{      
        for (i = 0; values.length > i; i++){
            missionObj[keys[i]] = values[i];
        }
        missions.push(missionObj);
        
    }
}

fs.rmSync(outputPath, { recursive: true, force: true });
  if (!fs.existsSync(outputPath)){
    fs.mkdirSync(outputPath);
}

fs.createReadStream(missionCSVFile)
.pipe(parse({ delimiter: ",", from_line: 1 }))
.on("data", function (row) {
    mapObject(row, missions, keysMissionCSVFile);
    
})
.on("end", function () {
    fs.createReadStream(agendaCSVfile)
        .pipe(parse({ delimiter: ",", from_line: 1 }))
        .on("data", function (row) {
            mapObject(row, goals, keysAgendaCSVfile);

        })
        .on("end", function () {
            fs.createReadStream(missionTypeCSVFile)
            .pipe(parse({ delimiter: ",", from_line: 1 }))
            .on("data", function (row) {
                mapObject(row, missionTypes, keysMissionTypeCSVFile)
                

            })
            .on("end",async function () {
                pages = await readInHTMLFiles();
                await downloadResources(missionTypes,"icon","");
                await downloadResources(goals,"icon","");
                await downloadResources(missions,"openGraphPic","og/");
                await removeExternalJS(pages);
                pages = await generateMissionPages(pages);
                await copyResourceFolder("./src/css",outputPath+"/css");
                await copyResourceFolder("./src/images",outputPath+"/images");
                await copyResourceFolder("./src/js",outputPath);
                await copyResourceFolder("./customjs",outputPath+"/js");
                await copyResourceFolder("./src/documents",outputPath+"/documents");
                await generateMissionsOnSearchPage(pages)
                
                
                //console.log(searchPage)
                
                writePageFiles(pages);
                

                
                
            })
    })
})

function generateMissionsOnSearchPage (pages){
    searchPage = pages.find(page => page.fileName === "sok-uppdrag.html")
    $ = cheerio.load(searchPage.content)

    $(".w-dyn-empty").first().remove();
    $(".w-dyn-empty").first().remove();
    $searchList = $("#w-node-f37b10d7-f1e7-4198-75d2-66972b73763c-203fa1f4").clone();
    
            
    for(i = 0; missionTypes.length > i; i++){
        $searchList.find("img").first().attr("src","./icons/"+missionTypes[i].Slug+".png");
        $searchList.find(".infoboxtext").first().text(missionTypes[i].Slug);
        $("#w-node-f37b10d7-f1e7-4198-75d2-66972b73763c-203fa1f4").parent().append($searchList.clone());
    }
    $("#w-node-f37b10d7-f1e7-4198-75d2-66972b73763c-203fa1f4").first().remove();

    $goalList = $("#w-node-_8a9c8b91-6b89-e0d5-6dfb-f103411ede5e-203fa1f4").clone();
    

    for(let i = 0 ; i < goals.length;i++){
        let lowObject = goals[i];
        let index = i;
        for(let ii = i ; ii < goals.length;ii++){ 
            if (Number(lowObject.number) > Number(goals[ii].number)){
                lowObject = goals[ii];
                index = ii;
            }
        }
        
        goals[index] = goals[i];
        goals[i] = lowObject;

    }
   


    for(i = 0; goals.length > i; i++){
        
        $goalList.find("img").first().attr("src","./icons/"+goals[i].Slug+".png");
        $goalList.find(".infoboxtext").first().text(goals[i].number);
        $goalList.attr("id",goals[i].Slug);
        $("#w-node-_8a9c8b91-6b89-e0d5-6dfb-f103411ede5e-203fa1f4").parent().append($goalList.clone());
    }
    $("#w-node-_8a9c8b91-6b89-e0d5-6dfb-f103411ede5e-203fa1f4").first().remove();


    $("head").append('<script src="./js/searchpage.js"></script>');
    sokDiv = $("#w-node-e761a2e7-d231-8853-8f3b-ea36e224967c-203fa1f4").clone();
    
    for(let mission of missions){
        //let mission = missions[index];  
      missionGoal = goals.find(goal => goal.Slug === mission.agendaMainGoal)
     

      
    
    sokDiv.attr('id', function(i, id){
        return id=mission.Slug;
       
    });
    
    
    $("#w-node-e761a2e7-d231-8853-8f3b-ea36e224967c-203fa1f4").parent().append(sokDiv.clone());
    $element = $("#"+mission.Slug);
    $element.find(".agenda-riktning-holder").attr("style","background-color:"+missionGoal.color)
    $element.find(".agenda-riktning-holder").find(".text-block-12").text(missionGoal.number)
    
    if(mission.agendaMainGoal === "inte-agenda-mal"){
        $element.find(".agendaholdercorner").first().remove()
    }
    $element.addClass(mission.gigType);
    $element.find(".linktilluppdrag").first().attr("href","./"+mission.Slug+".html");
    $element.find(".hj-lps-kare").text(mission.organization);
    $element.find("#w-node-_91cfcfb4-114e-cbb8-c88d-4912710bad3a-203fa1f4").text(mission.leadingQuestion);

    if(mission.scopeHours != ''){
        $element.find(".timeplacebox__text-timmar").first().text(mission.scopeHours);
    }else{
        $element.find(".infoboxtext.tim").first().addClass("w-condition-invisible")
    }
    if(mission.scopeDays != ''){
        $element.find(".timeplacebox__text-dagar").first().text(mission.scopeDays);
    }else{
        $element.find(".infoboxtext.days").first().addClass("w-condition-invisible")
    }
    if(mission.scopeMonths != ''){
        $element.find(".timeplacebox__text-months").first().text(mission.scopeMonths);
    }else{
        $element.find(".infoboxtext.months").first().addClass("w-condition-invisible")
    }
    $element.find(".omfattning_del2").first().find(".infoboxtext").text(mission.place)
    

    


    // möten

    adMeets($,$element,mission);


    
    $element.find(".typwrapper > img").first().attr("src", "./icons/"+mission.gigType+".png");
    $element.find(".typwrapper > .typ_text").first().text(mission.gigType);
    $element.find(".hidden_filter_typ").first().text(mission.gigType);
    $element.find(".hidden_filter_agenda").first().text(missionGoal.number);
    
    
    
}
        
    $(".uppdragwrapper > .w-dyn-list > .w-dyn-empty").first().remove();

    $(".uppdragwrapper > .w-dyn-list").first().append($(".empty_wrapper").first().clone());
    $(".empty_wrapper").first().remove();

    $("#w-node-e761a2e7-d231-8853-8f3b-ea36e224967c-203fa1f4").remove();
    
    searchPage.content = $.html(); 

    
console.log("finished generate pages");
}

function copyResourceFolder(src,dest){
    try {
        fse.copySync(src, dest, { overwrite: true })
        console.log('copy success!')
      } catch (err) {
        console.error(err)
      }
}
function writePageFiles (pages) {
    pages.forEach(page => {
        writeFile(outputPath+"/"+page.fileName, page.content)
    })
}


function adMeets($,$element,mission){ 
    $meets = $element.find(".del2");    
    $meets = $meets.find(".datumwrapper")
    j=1
    $meets.each(function(i, elm) {
        
        
        
        date = new Date(mission['meetup'+j+'Start']);
        dateEnd = new Date(mission['meetup'+j+'End']);
        durration = (dateEnd.getTime()- date.getTime()) /1000/60;
        
        $(elm).parent().parent().addClass("date-container");
        $(elm).parent().parent().attr("data-date",date);
        $(elm).parent().parent().attr("data-durration",durration);

        $(elm).find(".datum1 > h2").text(date.getDate());
        $(elm).find(".datum-2 > h6").text(date.toLocaleString('default', { month: 'short' }));
        j+=1

    });
}
function addDate(id,value, $){
    date = new Date(value);
    
    if(date != "Invalid Date"){
        
        $("#"+id).attr("date",value)
        $("#"+id).parent().attr("data-date",value)
        $("#"+id+" .datum1 .heading-5").text(date.getDate());
        $("#"+id+" .datum-2 h6").text(date.toLocaleString('default', { month: 'short' }));
        $("#"+id).parent().children(".tidwrapper").children(".tidwrapper_step2").children(".tid").children(".tidb-rjar").text(date.toLocaleString('default', {hour: '2-digit', minute:'2-digit'}))
    }else{
        $("#"+id).parent().remove().html();
    }
}

function generateMissionPages(pages) {
    
    pages.forEach(page => {
        
    })
    template = pages.find(page => page.fileName == "detail_uppdrag.html");
    missions.forEach(mission => {
        console.log(mission)
        missionGoal = goals.find(goal => goal.Slug === mission.agendaMainGoal);
        $ = cheerio.load(template.content);
        $('.wrapperorg').first().find('.infoboxtext').first().text(mission.organization)
        $('[property="og:image"]').attr('content', '/icons/og/'+mission.Slug+'.png');
        $('[property="og:description"]').attr("content", mission.leadingQuestion);

        $('[property="twitter:image"]').attr('content', '/icons/og/'+mission.Slug+'.png');
        $('[property="twitter:description"]').attr("content", mission.leadingQuestion);

        $("#w-node-_8d139b98-85f8-1212-9ee7-f6f31e982694-143fa1e9").attr("style","border-color:"+missionGoal.color)
        $("#w-node-f3f1f77b-67ed-e312-e2b8-46abffed82ea-143fa1e9").attr("src",missionGoal.icon)
        $("head").append('<script src="./js/searchpage.js"></script>');
        $(".agenda-riktning-holder").attr("style","background-color:"+missionGoal.color)
        $(".agenda-riktning-holder").find(".text-block-12").text(missionGoal.number)
        adMeets($,$("body").first(),mission)
                $("#w-node-ed31958f-1d6e-435d-a01f-614810bcfdbe-143fa1e9").text(mission.leadingQuestion); // drivande fråga
        
                
                $(".del3 p:first").text(mission.description);
    //            $("head").append('<script src="./js/searchpage.js" type="text/javascript"></script>')
                $("head").append('<script src="./js/custom.js" type="text/javascript"></script>');
                



                if(mission.leadingQuestion2.trim() == "" && mission.leadingQuestion3.trim() == "" && mission.leadingQuestion4.trim() == ""){
                    $(".flerdrivandefragorcontainer").first().addClass("w-condition-invisible")
                }else{
                    if(mission.leadingQuestion2 != ""){
                        $("#w-node-_96c320c8-5960-7f85-cabc-7f61c90d4df3-143fa1e9 .flerdrivandefragor").text(mission.leadingQuestion2);
                    }else{
                        $("#w-node-_96c320c8-5960-7f85-cabc-7f61c90d4df4-143fa1e9").addClass("w-condition-invisible")
                    }
    
                    if(mission.leadingQuestion3 != ""){
                        $("#w-node-_96c320c8-5960-7f85-cabc-7f61c90d4df4-143fa1e9 .flerdrivandefragor").text(mission.leadingQuestion3);
                    }else{
                        $("#w-node-_96c320c8-5960-7f85-cabc-7f61c90d4df4-143fa1e9").addClass("w-condition-invisible");
                    }
                    if(mission.leadingQuestion4 != ""){
                        $("#w-node-_96c320c8-5960-7f85-cabc-7f61c90d4df5-143fa1e9 .flerdrivandefragor").text(mission.leadingQuestion4);
                    }else{
                        $("#w-node-_96c320c8-5960-7f85-cabc-7f61c90d4df5-143fa1e9").addClass("w-condition-invisible");
                    }
                }

                

                $("#w-node-e6e47bdb-3235-702f-71d5-2baac340534f-143fa1e9").text(mission.leadingQuestionAgendaMainGoal);

                addDate("w-node-_04ddbe87-4467-ed20-10fa-77421e1d3b17-143fa1e9", mission.meetup1Start, $);
                addDate("w-node-_35c36f7e-1ec7-ea38-bd15-6f88883b4388-143fa1e9", mission.meetup1Start, $);
                addDate("w-node-_137bde93-799d-16bd-6fbe-bbb449bcfce4-143fa1e9", mission.meetup2Start, $);
                addDate("w-node-eedf0f5e-78ea-6b8d-67e3-298eee420f85-143fa1e9", mission.meetup2Start, $);
                addDate("w-node-_8bc2b2b1-e800-5dbe-0880-f653f7777320-143fa1e9", mission.meetup3Start, $);
                addDate("w-node-_2ccf6c9b-c29e-f81e-0a84-e41f84966cb8-143fa1e9", mission.meetup3Start, $);
                addDate("w-node-_4642e42a-d510-fe00-633e-069a3dd0e0a6-143fa1e9", mission.meetup4Start, $);
                addDate("w-node-ea542e4c-731e-9074-6346-6bbde765ba04-143fa1e9", mission.meetup4Start, $);
                
                $h6elements = $("h6")
                
                for(let i = 0;  $h6elements.length > i ; i++){
                    
                    if($($h6elements[i]).text() == "Mer om uppdragets omfattning"){
                        $($h6elements[i]).addClass("w-condition-invisible")
                    }

                }
                $(".typwrapper").find("img").first().attr("src", "./icons/"+mission.gigType+".png");
                $(".typwrapper").find(".typ_text").first().text(mission.gigType)
                
                
                if(mission.scopeHours != ''){
                    $(".timeplacebox__text-timmar").first().text(mission.scopeHours);
                }else{
                    $(".infoboxtext.tim").first().addClass("w-condition-invisible");
                }
                if(mission.scopeDays != ''){
                    $(".timeplacebox__text-dagar").first().text(mission.scopeDays);
                }else{
                    $(".infoboxtext.days").first().addClass("w-condition-invisible");
                }
                if(mission.scopeMonths != ''){
                    $(".timeplacebox__text-months").first().text(mission.scopeMonths);
                }else{
                    $(".infoboxtext.months").first().addClass("w-condition-invisible");
                }
                $(".omfattning_del2").first().find(".infoboxtext").text(mission.place);
                
                if(mission.agendaMainGoal === "inte-agenda-mal"){
                    $(".agendawrapper").first().addClass("w-condition-invisible");
                    $(".agendaholdercorner").first().addClass("w-condition-invisible");

                }

                for(i = 0; i<missionTypes.length; i++){
                    if(missionTypes[i][1] == mission.gigType){
                        $(".typwrapper").append('<div class="typ_text">'+missionTypes[i][0]+'</div>');
                    }
                }
        let missionObj = {};
        missionObj.fileName = mission.Slug+".html";
        missionObj.content = $.html();
        pages.push(missionObj);
    
    })
    
    
return pages
}

function download(url, dest, cb) {
    try {
        if (!fs.existsSync(dest)) {
            https.get(url, (res) => {
                res.on('data', (d) => {
                    fs.appendFileSync(dest, d); 
                  });
            }) 
          }
    } catch (error) {
        console.log(error)
    }

}

function removeExternalJS (array) {
    downloaded = []
    
    array.forEach(item => {
        let $ = cheerio.load(item.content);

        let scriptFilesElements = $("script")
        for(let i = 0; i<scriptFilesElements.length;i++){
            
            let src = $(scriptFilesElements[i]).attr("src");
            let fileName = src;
            if(fileName != undefined){
                while(fileName.indexOf("/")!=-1 ){
                
                    fileName = fileName.substring(fileName.indexOf("/")+1,fileName.length)
                }

                while(fileName.indexOf("?")!=-1){
                    
                    fileName = fileName.substring(0,fileName.indexOf("?"))
                    
                    
                }
            

            if(fileName != ""){
                
                $(scriptFilesElements[i]).attr("src",fileName)
                $(scriptFilesElements[i]).removeAttr("integrity")
                
                if (src.includes("http") && !downloaded.find(str => str === fileName)){
                    downloaded.push(fileName);
                    download(src, outputPath+"/"+fileName);
                }
            }
            
        }
        
       
        
        }
        item.content = $.html()
        
        
    })
}

function downloadResources (array,property,path){
    
    let dir = outputPath+"/icons/"+path;
    if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
  }
    array.forEach(obj => {
   
        downloadimg(obj[property], dir+obj.Slug+".png")
    })

}


var downloadimg = function(url, dest, cb) {
    try {
        var file = fs.createWriteStream(dest);
        
        https.get(url, function(response) {
          response.pipe(file);
          file.on('finish', function() {
            file.close(cb);
          });
        });
    } catch (error) {
        
    }

  }

function readInHTMLFiles(){
    let pages = []
    
    files = fs.readdirSync("./src");
    
    files.forEach( file => {
        
       let pageObj = {}
        if (Path.extname(file) =='.html'){
            
            pageObj.fileName = file;
            
            pageObj.content = fs.readFileSync('./src/'+file,{ encoding: 'utf8', flag: 'r' });
            pages.push(pageObj)
        }
    })
       return pages;
        
    }
        
   






















