FROM node:18.17.1-alpine3.18 AS build

ENV OUTPUT_PATH="./public"
WORKDIR /app
COPY . .
RUN npm install &&\
    npm run build
COPY package*.json /app/public
RUN cd public &&\
    npm install --production &&\
    cd ..



FROM nginxinc/nginx-unprivileged:1.25-alpine3.18-perl
RUN rm /etc/nginx/conf.d/default.conf
COPY --from=build /app/public /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf


EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]
